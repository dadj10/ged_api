package com.ebenyx.api;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ebenyx.entities.Utilisateur;
import com.ebenyx.repositoies.UtilisateurRepository;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;

import java.math.BigInteger;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api")
public class Api {

	@Value("${alfesco_server}")
	private String alfesco_server;

	@Value("${alfresco_port}")
	private String alfresco_port;

	@Value("${alfersco_login}")
	private String alfersco_login;

	@Value("${alfresco_pass}")
	private String alfresco_pass;

	@Autowired
	private UtilisateurRepository utilisateurRepos;

	@RequestMapping(value = "/init", method = RequestMethod.GET)
	public List<Utilisateur> init() {
		List<Utilisateur> utilisateurs = null;
		utilisateurs = utilisateurRepos.findAll();
		return utilisateurs;
	}

	@RequestMapping(value = "/do", method = RequestMethod.GET)
	public void _do() {
        
        // 192.168.8.118:8080
		try {
			// implémentation d'usine par défaut
	        SessionFactory factory = SessionFactoryImpl.newInstance();
	        // Map<String, String> parameter = new HashMap()<String, String>();
	        Map<String,String> parameter = new HashMap<>();

	        // informations d'identification de l'utilisateur
	        parameter.put(SessionParameter.USER, alfersco_login);
	        parameter.put(SessionParameter.PASSWORD, alfresco_pass);

	        // paramètres de connexion
	        parameter.put(SessionParameter.ATOMPUB_URL,
	                "http://"+alfesco_server+":"+alfresco_port+"/alfresco/api/-default-/public/cmis/versions/1.1/atom");
	        parameter.put(SessionParameter.BINDING_TYPE,
	                BindingType.ATOMPUB.value());

	        // créer une session
	        Session session = factory.getRepositories(parameter).get(0)
	                .createSession();
	        Folder root = session.getRootFolder();

	        // properties
	        Map<String, Object> properties = new HashMap<String, Object>();
	        properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
	        properties.put(PropertyIds.NAME, "test_adje"); // nom de dossier

	        // créer le dossier
	        Folder parent = root.createFolder(properties);

	        String name = "NewTextFile 3.txt";

	        // properties
	        Map<String, Object> properties2 = new HashMap<String, Object>();
	        properties2.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
	        properties2.put(PropertyIds.NAME, name);

	        // contenu
	        byte[] content = "Hello Codeur Ebenyx".getBytes();
	        InputStream stream = new ByteArrayInputStream(content);
	        ContentStream contentStream = new ContentStreamImpl(name,
	                BigInteger.valueOf(content.length), "text/plain", stream);

	        // créer une version majeure
	        Document newDoc = parent.createDocument(properties2, contentStream,
	                VersioningState.MAJOR);

	        System.out.println("DONE.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// TODO: handle finally clause
		}      
    }

}
